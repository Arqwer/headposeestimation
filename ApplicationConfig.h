//
// Created by arqwer on 21.08.15.
//

#ifndef HEADPOSEREALTIME_APPLICATIONCONFIG_H
#define HEADPOSEREALTIME_APPLICATIONCONFIG_H


class ApplicationConfig {
public:
    bool showRawImage;
    int camNumber;
    ApplicationConfig(){
        showRawImage = false;
        camNumber = 0;
    }
};


#endif //HEADPOSEREALTIME_APPLICATIONCONFIG_H
