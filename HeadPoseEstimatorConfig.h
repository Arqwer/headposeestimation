//
// Created by eigendeus on 05.08.15.
//

#ifndef HEADPOSEREALTIME_CONFIG_H
#define HEADPOSEREALTIME_CONFIG_H

#include <string>

using namespace std;

class HeadPoseEstimatorConfig {
public:
    int camNumber;
    string path_to_cascades;
    string left_eye_path, left_ear_path, nose_path, mouth_path, face_path, prface_path ;
    string right_eye_path, right_ear_path ;
    bool show_face_roi, show_frame;
    bool mirror;
    HeadPoseEstimatorConfig(){
        camNumber = 0;
        path_to_cascades = "/usr/share/opencv/haarcascades/";
        left_eye_path = path_to_cascades +  "/" + "haarcascade_lefteye_2splits.xml";
        right_eye_path = path_to_cascades + "/" + "haarcascade_righteye_2splits.xml";
        left_ear_path = path_to_cascades +  "/" + "haarcascade_mcs_leftear.xml";
        right_ear_path = path_to_cascades + "/" + "haarcascade_mcs_rightear.xml";
        nose_path = path_to_cascades +      "/" + "haarcascade_mcs_nose.xml";
        mouth_path = path_to_cascades +     "/" + "haarcascade_mcs_mouth.xml";
        face_path = path_to_cascades +      "/" + "haarcascade_frontalface_alt2.xml";
        prface_path = path_to_cascades +      "/" + "haarcascade_profileface.xml";

        show_face_roi = false;
        mirror = false;
    }
};


#endif //HEADPOSEREALTIME_CONFIG_H
