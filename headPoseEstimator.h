//
// Created by eigendeus on 05.08.15.
//

#ifndef HEADPOSEREALTIME_HEADPOSEESTIMATOR_H
#define HEADPOSEREALTIME_HEADPOSEESTIMATOR_H

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <utility>
#include <sys/time.h>
#include <unistd.h>

#include "HeadPoseEstimatorConfig.h"

using namespace std;
using namespace cv;

class headPoseEstimator {
public:
    HeadPoseEstimatorConfig cfg;
    CascadeClassifier lEye, rEye, mouth, lEar, rEar, nose, face,prface;
    Mat outputImage;
    Point2i lEyePt, rEyePt, mouthPt, nosePt, lEarPt, rEarPt;
    int pointCount;

    int initClassifiers();

    headPoseEstimator(){
        if(initClassifiers() != 0){
            cerr<<"Error loading cascades"<<endl;
        }
        pointCount = 0;
    }

    int analyse(const Mat &frame);

    int run();

    Point2i getlEyePos(const Mat &frame);
    Point2i getrEyePos(const Mat &frame);
    Point2i getlEarPos(const Mat &frame);
    Point2i getrEarPos(const Mat &frame);
    Point2i getMouthPos(const Mat &frame);
    Point2i getNosePos(const Mat &frame);

    Rect findFace(const Mat &frame);
};


#endif //HEADPOSEREALTIME_HEADPOSEESTIMATOR_H
