//
// Created by eigendeus on 05.08.15.
//

#include "headPoseEstimator.h"


Rect headPoseEstimator::findFace(const Mat &frame){
    Mat frame_gray;
    cvtColor( frame, frame_gray, CV_BGR2GRAY );
    equalizeHist( frame_gray, frame_gray );
    std::vector<Rect> rectangles;
    /* --> */face.detectMultiScale( frame_gray, rectangles, 1.1, 7, 0|CV_HAAR_SCALE_IMAGE, Size(30, 30) );
    if(rectangles.size()==0){
    /* --> */   prface.detectMultiScale( frame_gray, rectangles, 1.1, 3, 0|CV_HAAR_SCALE_IMAGE, Size(30, 30) );
    }
    if(rectangles.size()==0){
        return Rect(-1,-1,-1,-1);
    }
    Rect res = rectangles[0];
    res.height *=1.2;
    res.width *=1.6;
    res.x -= res.width*0.2;
    res.y -= res.height*0.1;
    res &= Rect(0,0,frame.cols,frame.rows);
    return res;
}

Point2i getPosByCascade(const Mat &frame, CascadeClassifier & classif, int minNeighbours = 5){
    Mat frame_gray;
    cvtColor( frame, frame_gray, CV_BGR2GRAY );
    equalizeHist( frame_gray, frame_gray );
    std::vector<Rect> rectangles;
    /* --> */classif.detectMultiScale( frame_gray, rectangles, 1.1, minNeighbours, 0|CV_HAAR_SCALE_IMAGE, Size(10, 10) );
    if(rectangles.size()==0){
        return Point2i(-1,-1);
    }
    Point2i res;
    res.x = rectangles[0].x + rectangles[0].width/2;
    res.y = rectangles[0].y + rectangles[0].height/2;
    return res;
}

Point2i headPoseEstimator::getlEyePos(const Mat &frame) {
    return getPosByCascade(frame, lEye);
}

Point2i headPoseEstimator::getrEyePos(const Mat &frame) {
    return getPosByCascade(frame, rEye);
}

Point2i headPoseEstimator::getlEarPos(const Mat &frame) {
    return getPosByCascade(frame, lEar, 2);
}

Point2i headPoseEstimator::getrEarPos(const Mat &frame) {
    return getPosByCascade(frame, rEar, 2);
}

Point2i headPoseEstimator::getMouthPos(const Mat &frame) {
    return getPosByCascade(frame, mouth);
}

Point2i headPoseEstimator::getNosePos(const Mat &frame) {
    return getPosByCascade(frame, nose);
}

int headPoseEstimator::analyse(const Mat &frame) {
//    cout<<"AAAA"<<endl;
//    medianBlur(frame,frame,3);
    pointCount = 0;
    outputImage = frame.clone();
    Rect face_r = findFace(frame);
    int width = face_r.width;
    int height = face_r.height  ;
    Rect face_bottom(    face_r.x + width/4, face_r.y+2*height/3, width/2,   height/3   );
    Rect face_top_right( face_r.x+width/10,  face_r.y,            width*0.45,   2*height/3 );
    Rect face_top_left(  face_r.x+width*0.45,face_r.y,            width*0.45,   2*height/3 );
    Rect face_center(face_r.x + width*0.3, face_r.y + height/3, width*0.4,height*3/6);
    if(cfg.show_face_roi) {
        rectangle(outputImage, face_r, Scalar(100, 0, 0), 2, 1, 0);
        rectangle(outputImage, face_top_left, Scalar(50, 50, 200), 2, 1, 0);
        rectangle(outputImage, face_top_right, Scalar(50, 200, 50), 2, 1, 0);
        rectangle(outputImage, face_bottom, Scalar(100, 00, 100), 2, 1, 0);
        rectangle(outputImage, face_center, Scalar(100, 100, 0), 2, 1, 0);
    }

    if( (face_r | Rect(0,0,frame.cols,frame.rows)) != Rect(0,0,frame.cols,frame.rows)) {
        return -1;
    }
    Mat face_m = frame(face_r);
    vector<Point2i > positions_face;

//    static struct timeval frameStartsA,frameEndsA;
//    gettimeofday(&frameStartsA, NULL);

    positions_face.push_back( getlEyePos (frame(face_top_left)));
    positions_face.push_back( getrEyePos (frame(face_top_right)));
    positions_face.push_back( getlEarPos (frame(face_r)));
    positions_face.push_back( getrEarPos (frame(face_r)));
    positions_face.push_back( getNosePos (frame(face_center)));
    positions_face.push_back( getMouthPos(frame(face_bottom)));

    vector<Point2i > positions;
    positions.resize( positions_face.size() );
    for(unsigned int i = 0; i < positions_face.size(); ++i){
        if(positions_face[i] != Point2i(-1, -1)){
            pointCount++;
        }
        if(positions_face[i] != Point2i(-1,-1)){
            switch(i){
                case 0:
                    positions[i] = positions_face[i] + Point2i(face_top_left.x, face_top_left.y);
                break;
                case 1:
                    positions[i] = positions_face[i] + Point2i(face_top_right.x, face_top_right.y);
                break;
                case 2:
                    positions[i] = positions_face[i] + Point2i(face_r.x, face_r.y);
                break;
                case 3:
                    positions[i] = positions_face[i] + Point2i(face_r.x, face_r.y);
                break;
                case 4:
                    positions[i] = positions_face[i] + Point2i(face_center.x, face_center.y); //nose
                break;
                case 5:
                    positions[i] = positions_face[i] + Point2i(face_bottom.x, face_bottom.y); //mouth
                break;
            }
        }else{
            positions[i] = Point2i(0,0);
        }
//        gettimeofday(&frameEndsA, NULL);
//        cout<<"Haar time: "<<(frameEndsA.tv_usec - frameStartsA.tv_usec)/1000000.0<<endl;
//        cout<<"FPSA: "<<1000000.0/(frameEndsA.tv_usec - frameStartsA.tv_usec)<<endl;
    }

    lEyePt = positions[0];
    rEyePt = positions[1];
    lEarPt = positions[2];
    rEarPt = positions[3];
    nosePt = positions[4];
    mouthPt = positions[5];

    for(unsigned int i = 0; i < positions.size(); i++) {
        auto &v = positions[i];
        auto color = Scalar(0,0,0);
        switch(i){
            case 0: color = Scalar(0,50,200); break;  //leye
            case 1: color = Scalar(0,200,0); break; //reye
            case 2:  //lear
            case 3: color = Scalar(0,100,200); break; //rEar
            case 4: color = Scalar(200,0,0); break; //nose
            case 5: color = Scalar(200,200,0); break; //mouth
        }
        line(outputImage, v, v + Point2i(1, 0), color, 3, 1, 0);
    }
    return 0;
}

int headPoseEstimator::run() {
    Mat frame;
    VideoCapture cap(cfg.camNumber);
    if(!cap.isOpened()){// check if we succeeded
        cerr<<"Can't connect to camera"<<endl;
        return -1;
    }

    while(true){
        cap>>frame;
        int c = waitKey(10);
        if( (char)c == 27 ) { break; }
        analyse(frame);
        if(cfg.mirror) {
            Mat mirrowedImage;
            flip(outputImage, mirrowedImage, 1);
            imshow("Camera", mirrowedImage);
        }
        else{
            imshow("Camera", outputImage);
        }
    }
    return 0;
}

int headPoseEstimator::initClassifiers() {
    if( !lEar.load( cfg.left_ear_path ) ){
        cerr<<"--(!)Error loading cascade "<<cfg.left_ear_path<<endl;
        return -1;
    }
    if( !rEar.load( cfg.right_ear_path) ){
        cerr<<"--(!)Error loading cascade "<<cfg.right_ear_path<<endl;
        return -1;
    }
    if( !lEye.load( cfg.left_eye_path ) ){
        cerr<<"--(!)Error loading cascade "<<cfg.left_eye_path<<endl;
        return -1;
    }
    if( !rEye.load( cfg.right_eye_path ) ){
        cerr<<"--(!)Error loading cascade "<<cfg.right_eye_path<<endl;
        return -1;
    }
    if( !mouth.load( cfg.mouth_path ) ){
        cerr<<"--(!)Error loading cascade "<<cfg.mouth_path<<endl;
        return -1;
    }
    if( !nose.load( cfg.nose_path ) ){
        cerr<<"--(!)Error loading cascade "<<cfg.nose_path<<endl;
        return -1;
    }
    if( !face.load( cfg.face_path ) ){
        cerr<<"--(!)Error loading cascade "<<cfg.face_path<<endl;
        return -1;
    }
    if( !prface.load( cfg.prface_path ) ){
        cerr<<"--(!)Error loading cascade "<<cfg.prface_path<<endl;
        return -1;
    }
    return 0;
}
