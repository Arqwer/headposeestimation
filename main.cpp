// HeadPose.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"

#include "cv.h"
#include "highgui.h"
#include "headPoseEstimator.h"
#include "ApplicationConfig.h"

using namespace cv;

#include <vector>
#include <iostream>
#include <fstream>

using namespace std;


//#include "headPoseEstimator.h"
VideoCapture cap;
vector<Point3f > modelPoints;
headPoseEstimator hpEst;


#if defined(__APPLE__)
#  include <OpenGL/gl.h>
#  include <OpenGL/glu.h>
#elif defined(__linux__) || defined(__MINGW32__) || defined(WIN32)
#  include <GL/gl.h>
#  include <GL/glu.h>
#else
#  include <gl.h>
#endif

#include "glm.h"
#include "OGL_OCV_common.h"

void loadNext();
void calcTransformations(Mat &image_points, Mat &object_points, Size mat_size);

const GLfloat light_ambient[]  = { 0.0f, 0.0f, 0.0f, 1.0f };
const GLfloat light_diffuse[]  = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_position[] = { 0.0f, 0.0f, 1.0f, 0.0f };

const GLfloat mat_ambient[]    = { 0.7f, 0.7f, 0.7f, 1.0f };
const GLfloat mat_diffuse[]    = { 0.8f, 0.8f, 0.8f, 1.0f };
const GLfloat mat_specular[]   = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat high_shininess[] = { 100.0f };

double rot[9] = {0};
GLuint textureID;
Mat backPxls;
vector<double> rv(3), tv(3);
Mat rvec(rv),tvec(tv);
Mat camMatrix;

OpenCVGLTexture imgTex,imgWithDrawing;

GLMmodel* head_obj;

void saveOpenGLBuffer() {
    static unsigned int opengl_buffer_num = 0;

    int vPort[4]; glGetIntegerv(GL_VIEWPORT, vPort);
    Mat_<Vec3b> opengl_image(vPort[3],vPort[2]);
    {
        Mat_<Vec4b> opengl_image_4b(vPort[3],vPort[2]);
        glReadPixels(0, 0, vPort[2], vPort[3], GL_BGRA, GL_UNSIGNED_BYTE, opengl_image_4b.data);
        flip(opengl_image_4b,opengl_image_4b,0);
        mixChannels(&opengl_image_4b, 1, &opengl_image, 1, &(Vec6i(0,0,1,1,2,2)[0]), 3);
    }
    stringstream ss; ss << "opengl_buffer_" << opengl_buffer_num++ << ".jpg";
    imwrite(ss.str(), opengl_image);
}


void resize(int width, int height)
{
    const float ar = (float) width / (float) height;

    glViewport(0, 0, width, height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    //glFrustum(-ar, ar, -1.0, 1.0, 2.0, 100.0);
    gluPerspective(47,1.0,0.01,1000.0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

int __w=250,__h=250;

void key(unsigned char key, int x, int y)
{
    if(key == 27){
        exit(0);
    }

    glutPostRedisplay();
}

void idle(void)
{
    static struct timeval frameStarts,frameEnds;
    double tickPerSecond = cvGetTickFrequency()*10e6;
    auto then = cvGetTickCount();
    loadNext();
    auto now = cvGetTickCount();
    cout<<"FPS: "<<1.0/(now-then)/tickPerSecond<<endl;
    glutPostRedisplay();
}




void myGLinit() {
//    glutSetOption ( GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_CONTINUE_EXECUTION ) ;

    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);


    glShadeModel(GL_SMOOTH);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE);
    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial ( GL_FRONT, GL_AMBIENT_AND_DIFFUSE );

    glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE,  light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);

    glMaterialfv(GL_FRONT, GL_AMBIENT,   mat_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE,   mat_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR,  mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, high_shininess);

    glEnable(GL_LIGHTING);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

}

void drawAxes() {

    //Z = red
    glPushMatrix();
    glRotated(180,0,1,0);
    glColor4d(1,0,0,0.5);
//	glutSolidCylinder(0.05,1,15,20);
    glBegin(GL_LINES);
    glVertex3d(0, 0, 0); glVertex3d(0, 0, 1);
    glEnd();
    glTranslated(0,0,1);
    glScaled(.1,.1,.1);
    glutSolidTetrahedron();
    glPopMatrix();

    //Y = green
    glPushMatrix();
    glRotated(-90,1,0,0);
    glColor4d(0,1,0,0.5);
//	glutSolidCylinder(0.05,1,15,20);
    glBegin(GL_LINES);
    glVertex3d(0, 0, 0); glVertex3d(0, 0, 1);
    glEnd();
    glTranslated(0,0,1);
    glScaled(.1,.1,.1);
    glutSolidTetrahedron();
    glPopMatrix();

    //X = blue
    glPushMatrix();
    glRotated(-90,0,1,0);
    glColor4d(0,0,1,0.5);
//	glutSolidCylinder(0.05,1,15,20);
    glBegin(GL_LINES);
    glVertex3d(0, 0, 0); glVertex3d(0, 0, 1);
    glEnd();
    glTranslated(0,0,1);
    glScaled(.1,.1,.1);
    glutSolidTetrahedron();
    glPopMatrix();
}

void display(void)
{
    // draw the image in the back
    int vPort[4]; glGetIntegerv(GL_VIEWPORT, vPort);
    glEnable2D();
    drawOpenCVImageInGL(imgTex);
    glTranslated(vPort[2]/2.0, 0, 0);
    drawOpenCVImageInGL(imgWithDrawing);
    glDisable2D();

    glClear(GL_DEPTH_BUFFER_BIT); // we want to draw stuff over the image

    // draw only on left part
    glViewport(0, 0, vPort[2]/2, vPort[3]);

    glPushMatrix();

    gluLookAt(0,0,0,0,0,1,0,-1,0);

    // put the object in the right position in space
    Vec3d tvv(tv[0],tv[1],tv[2]);
    glTranslated(tvv[0], tvv[1], tvv[2]);

    // rotate it
    double _d[16] = {	rot[0],rot[1],rot[2],0,
                         rot[3],rot[4],rot[5],0,
                         rot[6],rot[7],rot[8],0,
                         0,	   0,	  0		,1};
    glMultMatrixd(_d);

    // draw the 3D head model
    glColor4f(1, 1, 1,0.75);
    glmDraw(head_obj, GLM_SMOOTH);

    //----------Axes
    glScaled(50, 50, 50);
    drawAxes();
    //----------End axes

    glPopMatrix();

    // restore to looking at complete viewport
    glViewport(0, 0, vPort[2], vPort[3]);

    glutSwapBuffers();
}

void init_opengl(int argc,char** argv) {
    glutInitWindowSize(1367,512);
    glutInitWindowPosition(40,40);
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH); // | GLUT_MULTISAMPLE
    glutCreateWindow("head pose");

    myGLinit();

    glutReshapeFunc(resize);
    glutDisplayFunc(display);
    glutKeyboardFunc(key);
    //glutSpecialFunc(special);
    glutIdleFunc(idle);
}

int start_opengl() {

    glutMainLoop();

    return 1;
}

//Mat op;

void loadNext() {
    Mat img_disp, img_cam;

    cap >>img_cam;
    hpEst.analyse(img_cam);

    img_disp = hpEst.outputImage;

    Point2i lEye, rEye, nose, mouth;

    lEye = hpEst.lEyePt;
    rEye = hpEst.rEyePt;
    nose = hpEst.nosePt;
    mouth = hpEst.mouthPt;

    vector<Point2f > imagePoints;
    imagePoints.push_back(lEye);
    imagePoints.push_back(rEye);
    imagePoints.push_back(nose);
    imagePoints.push_back(mouth);

    imgTex.set(img_disp); //TODO: what if different size??

    circle(img_disp,imagePoints[0],2,Scalar(0,0,200),CV_FILLED);
    circle(img_disp,imagePoints[1],2,Scalar(0,200,0),CV_FILLED);
    circle(img_disp,imagePoints[2],2,Scalar(200,0,0),CV_FILLED);
    circle(img_disp,imagePoints[3],2,Scalar(255,0,255),CV_FILLED);

    if(hpEst.pointCount >= 4) {
        Mat mat_image_points(imagePoints);
        Mat mat_object_points(modelPoints);
        calcTransformations(mat_image_points, mat_object_points, img_disp.size());
    }

    imgWithDrawing.set(img_disp);
}

void calcTransformations(Mat &image_points, Mat &object_points, Size mat_size) {

    int max_dimension = MAX(mat_size.height, mat_size.width);
    camMatrix = (Mat_<double>(3,3) <<
            max_dimension, 0, mat_size.width/2.0,
            0, max_dimension, mat_size.height/2.0,
            0,	0,	1.0);

    double _dist_coefs[] = {0,0,0,0};
    solvePnP(object_points, image_points,camMatrix,Mat(1,4,CV_64FC1, _dist_coefs),rvec,tvec,false,CV_P3P);
    Mat rotM(3,3,CV_64FC1,rot);
    Rodrigues(rvec,rotM);

    rotM = rotM.t();// transpose to conform with majorness of opengl matrix
    //operations on this matrix changes global var rot

}


int main(int argc, char** argv)
{
    static ApplicationConfig appConfig;
    cap.open(appConfig.camNumber);
    if(!cap.isOpened()){// check if we succeeded
        cerr<<"Can't connect to camera"<<endl;
        return -1;
    }

    modelPoints.push_back(Point3f(72.0602,115.898,21.8234));	// l eye (v 0)
    modelPoints.push_back(Point3f(5.37427,115.322,22.7776));	// r eye (v 314)
    modelPoints.push_back(Point3f(36.8301,78.3185,52.0345));	//nose (v 1879)
    modelPoints.push_back(Point3f(36.5161,43.0115,39.9301));	//  mouth (v 1502)
//    modelPoints.push_back(Point3f(58.1825,51.0115,29.6224));	// r mouth (v 695)

//    modelPoints.push_back(Point3f(-61.8886,127.797,-89.4523));	// l ear (v 2011)
//    modelPoints.push_back(Point3f(127.603,126.9,-83.9129));		// r ear (v 1138)

    head_obj = glmReadOBJ("head-obj.obj");

    rvec = Mat(rv);
    double _d[9] = {1,0,0,
                    0,-1,0,
                    0,0,-1};
    Rodrigues(Mat(3,3,CV_64FC1,_d),rvec);
    tv[0]=0;tv[1]=0;tv[2]=1;
    tvec = Mat(tv);

    camMatrix = Mat(3,3,CV_64FC1);

    init_opengl(argc, argv); // get GL context, for loading textures

    // prepare OpenCV-OpenGL images
    imgTex = MakeOpenCVGLTexture(Mat());
    imgWithDrawing = MakeOpenCVGLTexture(Mat());

    loadNext();

    start_opengl();

    return 0;
}


